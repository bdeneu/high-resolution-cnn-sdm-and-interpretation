import torch
import json
import random
import numpy as np
import pandas as pd
from sklearn import metrics
import matplotlib.pyplot as plt

# SETTINGS
N_LABELS = 31435

#predictions_cnn = np.load("/home/bdeneu/GLC20/post_challenge/activations_logits_test_official_all_indexed.npy")
#predictions_rf = np.load("/home/bdeneu/GLC20/post_challenge/predictions_rfenv_test_official_all_indexed.npy")

#test_labels = pd.read_csv("/home/bdeneu/GLC20/post_challenge/occurrences_all_test_label.csv", header='infer', sep=';', low_memory=False).to_numpy()[:, -1]

predictions_cnn = np.load("activations_logits_test_official_all_indexed.npy")
predictions_rf = np.load("predictions_rfenv_test_official_all_indexed.npy")

test_labels = pd.read_csv("occurrences_all_test_label.csv", header='infer', sep=';', low_memory=False).to_numpy()[:, -1]


def selectPseudoAbsence(sp, nb_presence, predictions, labels, selection_type, weight=None, min_abs=100):
    if selection_type == "sp":
        # select species for pseudo-absence
        others = []
        pseudo_abscence_predictions = []
        removed_species = [sp]
        while len(others) < max(nb_presence, min_abs):
            # need to have at least as pseudo-absence as presence
            other_sp = random.randint(0, N_LABELS)
            while other_sp == sp:
                # select a random species
                other_sp = random.randint(0, N_LABELS)
            list_occs = predictions[labels == other_sp]
            if len(list_occs) > 0:
                random.shuffle(list_occs)
                others.append(other_sp)
                pseudo_abscence_predictions.append(list_occs[0][sp])
        others = np.asarray(others)
        pseudo_abscence_predictions = np.asarray(pseudo_abscence_predictions)
        #rand = np.random.choice(others.shape[0], size=nb_presence, replace=False)
        #pseudo_abscence_predictions = pseudo_abscence_predictions[rand][:, sp]
        #pseudo_abscence = others[rand]
        pseudo_abscence = others
    elif selection_type == "w":
        others = labels[labels != sp]
        pseudo_abscence_predictions = predictions[labels != sp]
        proba = weight[labels != sp] / np.sum(weight[labels != sp])
        rand = np.random.choice(others.shape[0], size=nb_presence, replace=False, p=proba)
        pseudo_abscence_predictions = pseudo_abscence_predictions[rand][:, sp]
        pseudo_abscence = others[rand]
    else:
        # randomly select pseudo-absence
        others = labels[labels != sp]
        pseudo_abscence_predictions = predictions[labels != sp]
        rand = np.random.choice(others.shape[0], size=nb_presence, replace=False)
        pseudo_abscence_predictions = pseudo_abscence_predictions[rand][:, sp]
        pseudo_abscence = others[rand]
    return pseudo_abscence_predictions, pseudo_abscence


def AUCBySpecies(predictions, labels, selection_type):
    list_score = []
    list_tts = []
    compte = 0
    nb_fig = 0

    unique, count = np.unique(labels, return_counts=True)
    weight = np.asarray([1 / count[np.argwhere(unique == l)[0, 0]] for l in labels])

    for SP in range(N_LABELS):
        presence = labels[labels == SP]
        if 1 <= presence.shape[0]:
            # species in the test set
            compte += 1
            presence_predictions = predictions[labels == SP][:, SP]

            pseudo_abscence_predictions, pseudo_abscence = selectPseudoAbsence(SP, presence.shape[0], predictions, labels, selection_type, weight=weight)

            points = np.concatenate((presence, pseudo_abscence))
            points_predictions = np.concatenate((presence_predictions, pseudo_abscence_predictions))

            fpr, tpr, thresholds = metrics.roc_curve(points, points_predictions, pos_label=SP)

            """
            if presence.shape[0] == 1:
                plt.plot(fpr, tpr)
                plt.ylabel('true positive rate')
                plt.xlabel('false positive rate')
                plt.savefig("roc_curve_"+str(nb_fig)+".png")
                nb_fig += 1
                plt.clf()
            """
            score = metrics.auc(fpr, tpr)
            list_score.append(score)
            list_tts.append(score)

    return list_score

def AUC(species, predictions, labels, selection_type):
    list_score = []
    list_tts = []
    compte = 0
    nb_fig = 0

    #unique, count = np.unique(labels, return_counts=True)
    #weight = np.asarray([1 / count[np.argwhere(unique == l)[0, 0]] for l in labels])

    for sp in species:
        presence = labels[labels == sp]
        print(sp, presence.shape[0])
        if presence.shape[0] > 0:
            # species in the test set
            compte += 1
            presence_predictions = predictions[labels == sp][:, sp]

            pseudo_abscence_predictions, pseudo_abscence = selectPseudoAbsence(sp, presence.shape[0], predictions, labels, selection_type, weight=None)

            points = np.concatenate((presence, pseudo_abscence))
            points_predictions = np.concatenate((presence_predictions, pseudo_abscence_predictions))

            fpr, tpr, thresholds = metrics.roc_curve(points, points_predictions, pos_label=sp)

            """
            if presence.shape[0] == 1:
                plt.plot(fpr, tpr)
                plt.ylabel('true positive rate')
                plt.xlabel('false positive rate')
                plt.savefig("roc_curve_"+str(nb_fig)+".png")
                nb_fig += 1
                plt.clf()
            """
            score = metrics.auc(fpr, tpr)
            list_score.append(score)
            list_tts.append(score)

    return list_score


def AUCAll(predictions, labels, selection_type):
    points = []
    points_predictions = []

    unique, count = np.unique(labels, return_counts=True)
    weight = np.asarray([1 / count[np.argwhere(unique == l)[0, 0]] for l in labels])

    for SP in range(N_LABELS):
        presence = labels[labels == SP]
        if 1 <= presence.shape[0]:
            presence_predictions = predictions[labels == SP][:, SP]

            pseudo_abscence_predictions, _ = selectPseudoAbsence(SP, presence.shape[0], predictions, labels, selection_type, weight=weight)

            presence_one = [1] * presence.shape[0]
            pseudo_abscence = [0] * presence.shape[0]

            points.extend(presence_one)
            points.extend(pseudo_abscence)
            points_predictions.extend(presence_predictions.tolist())
            points_predictions.extend(pseudo_abscence_predictions.tolist())

    fpr, tpr, thresholds = metrics.roc_curve(points, points_predictions)
    score = metrics.auc(fpr, tpr)

    return score


def AUCMulti(predictions, labels):
    # transform to keep only species in the test set
    predictions = predictions[:, np.unique(labels)]
    predictions = predictions/predictions.sum(axis=1, keepdims=1)
    return metrics.roc_auc_score(labels, predictions, multi_class='ovo', labels=np.unique(labels))


SELECT = "sp"
print("--------------- AUC spe ---------------")
list_score_rf = np.sort(AUCBySpecies(predictions_rf, test_labels, SELECT))
list_score_nn = np.sort(AUCBySpecies(predictions_cnn, test_labels, SELECT))
print("cnn:", np.mean(list_score_nn))
print("rf:", np.mean(list_score_rf))

print("cnn:", AUC([932, 669, 1003, 125], predictions_cnn, test_labels, SELECT))
print("rf:", AUC([932, 669, 1003, 125], predictions_rf, test_labels, SELECT))

plt.boxplot([list_score_nn, list_score_rf])
plt.ylabel('auc')
plt.savefig('/home/bdeneu/GLC20/post_challenge/auc_box_official_test_fr.png')
plt.clf()
plt.plot(list_score_nn)
plt.plot(list_score_rf)
plt.ylabel('auc')
plt.savefig('/home/bdeneu/GLC20/post_challenge/auc_curve_official_test_fr.png')
