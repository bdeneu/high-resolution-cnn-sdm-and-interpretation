import math
import matplotlib.pyplot as plt
from matplotlib import cm
from ds_engine.util.path import output_path
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from PIL import Image, ImageFilter
from sklearn.neighbors import NearestNeighbors
from pyproj import Transformer, Proj
import pandas as pd

mean_size = 1
black = False
colors = ((0, 0, 255, 255, 0), (0, 1, 255, 0, 0), (1, 0, 0, 255, 0), (1, 1, 0, 0, 255))

features_embedded = np.load(output_path("tsne_array_random_pca_big.npy"))
ids = np.load(output_path("tsne_random_occ_ids_pca_big.npy"))
features_ids = np.concatenate((np.expand_dims(ids, axis=1), features_embedded), axis=1)
features_ids_dict = {f[0]: [f[1], f[2]] for f in features_ids}
print(features_ids)

df_data = pd.read_csv("/home/benjamin/sdm_deploy/experiments/latefusion_20200609150900/occurrences_fr_train.csv", header='infer', sep=';', low_memory=False)
np_data = df_data.to_numpy()
np_data_new = []
for d in np_data:
    if d[0] in ids:
        np_data_new.append(d)
np_data = np.asarray(np_data_new)
np_data_ids = np.squeeze(np_data[:, 0])
np_data = np_data[:, 1:3]
print(np_data)

df_grid = pd.read_csv("/home/benjamin/sdm_deploy/experiments/latefusion_20200609150900/grid_points_1km.csv", header='infer', sep=';', low_memory=False)
np_grid = df_grid.to_numpy()
np_grid_ids = np.squeeze(np_grid[:, 2])
np_grid = np_grid[:, 0:2]
print(np_grid)

in_proj, out_proj = Proj(init='epsg:4326'), Proj(init='epsg:2154')
transformer = Transformer.from_proj(in_proj, out_proj)


def coord_transform(a):
    return transformer.transform(a[1], a[0])


np_data = np.apply_along_axis(coord_transform, 1, np_data)
np_grid = np.apply_along_axis(coord_transform, 1, np_grid)

nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(np_data)
distances, indices = nbrs.kneighbors(np_grid)

list_labels = []
all_features = []
for i, idx in enumerate(np_grid_ids):
    list_labels.append(idx)
    ft = []
    for id_occ in np_data_ids[indices[i]]:
        id_occ = int(id_occ)
        ft.append(features_ids_dict[id_occ])
    #print("------------------------------------------------")
    #print(ft)
    ft = np.asarray(ft)
    #print(ft)
    ft = np.mean(ft, axis=0)
    #print(ft)
    all_features.append(ft)
    #print(all_features)
    #exit()
map_ids = np.asarray(list_labels)
all_features = np.asarray(all_features)

def bilinear_interpolation(x, y, colors):
    # See formula at:  http://en.wikipedia.org/wiki/Bilinear_interpolation

    points = sorted(colors)               # order points by x, then by y
    (x1, y1, q11r, q11g, q11b), (_x1, y2, q12r, q12g, q12b), (x2, _y1, q21r, q21g, q21b), (_x2, _y2, q22r, q22g, q22b) = points

    if x1 != _x1 or x2 != _x2 or y1 != _y1 or y2 != _y2:
        raise ValueError('points do not form a rectangle')
    if not x1 <= x <= x2 or not y1 <= y <= y2:
        raise ValueError('(x, y) not within the rectangle')

    r = (q11r * (x2 - x) * (y2 - y) +
            q21r * (x - x1) * (y2 - y) +
            q12r * (x2 - x) * (y - y1) +
            q22r * (x - x1) * (y - y1)
           ) / ((x2 - x1) * (y2 - y1) + 0.0)
    g = (q11g * (x2 - x) * (y2 - y) +
         q21g * (x - x1) * (y2 - y) +
         q12g * (x2 - x) * (y - y1) +
         q22g * (x - x1) * (y - y1)
         ) / ((x2 - x1) * (y2 - y1) + 0.0)
    b = (q11b * (x2 - x) * (y2 - y) +
         q21b * (x - x1) * (y2 - y) +
         q12b * (x2 - x) * (y - y1) +
         q22b * (x - x1) * (y - y1)
         ) / ((x2 - x1) * (y2 - y1) + 0.0)

    d = 2 * (math.sqrt((0.5 - x) ** 2 + (0.5 - y) ** 2))

    # return np.min((r * d, 255)), np.min((g * d, 255)), np.min((b * d, 255))
    d = np.min((d, 1))
    return cm.plasma(d)[0] * 255, cm.plasma(d)[1] * 255, cm.plasma(d)[2] * 255



all_features = MinMaxScaler(feature_range=(0, 1)).fit_transform(all_features)
featured_transform = all_features

pos = []
max_x = 0
max_y = 0
for id_ in map_ids:
    x, y = id_.split("_")
    x, y = int(x), int(y)
    pos.append((x, y))
    if x > max_x:
        max_x = x
    if y > max_y:
        max_y = y
size = max(max_x + 1, max_y + 1)
while size % mean_size != 0:
    size += 1
act_map = np.full((size, size, 4), np.nan)

for k, act in enumerate(all_features):
    x, y = pos[k][0], pos[k][1]
    act_map[x, y, :3] = bilinear_interpolation(act[0], act[1], colors)
    #act_map[x, y, :2] = act
    act_map[x, y, 3] = 255

if mean_size != 1:
    height, width = act_map.shape[0], act_map.shape[1]
    act_map = np.nanmean(np.split(np.nanmean(np.split(act_map, width // mean_size, axis=0), axis=1),
                                  height // mean_size, axis=1), axis=2)

    act_map = np.fliplr(np.rot90(act_map, 3))

print(act_map)
masked_map = np.nan_to_num(act_map, nan=255)
print(masked_map)
masked_map = masked_map.astype(np.uint8)
if black:
    masked_map[:, :, 3] = 255

img = Image.fromarray(masked_map, mode="RGBA")
#img = img.filter(ImageFilter.GaussianBlur(radius=4))
img.save(output_path("tsne_visu_"+str(mean_size)+"km_2D_nn.png"))