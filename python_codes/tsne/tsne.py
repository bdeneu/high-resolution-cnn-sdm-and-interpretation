import math
import torch
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
#from ds.data.loader import occurrence_loader
from engine_import.occurrence_loader import occurrence_loader
from ds_engine import engine_configuration
from ds.ml.neural.checkpoints import load_or_create
from ds.ml.neural.supervised import fit
from ds import logging as log

from ds_engine import engine_configuration
from ds_engine.memory import get_parameter

from dataset import DatasetGLC20
from export import export
from models import InceptionEnv

from experiments.configs.inception import model_params, validation_params, training_params, optim_params
from ds.data.util.index import get_index
from ds_engine.util.path import output_path

import numpy as np
import matplotlib
from matplotlib import pyplot as plt

TSNE_SIZE = 3
PCA_SIZE = 50

source = 'glc20_gardiole'
engine_configuration.previous_execution = True

_, _, test = occurrence_loader(
    DatasetGLC20, source=source, validation_size=0., test_size=1., label_name=None,
    latitude='Latitude', longitude='Longitude', sep=';', use_rasters=False
)
model_params['n_input'] = test[0][0].shape[0]
model_params['n_labels'] = 31180
#model_params['last_layer'] = False
model_params['logit'] = True

model = load_or_create(model_class=InceptionEnv, model_params=model_params)
model.eval()

log.info('Dataset and model created... Exporting the results...')

index = get_index(output_path('index.json'))

"""
df_gt = pd.read_csv(output_path("ground_truth.csv"), header='infer', sep=';', low_memory=False)
np_gt = df_gt.to_numpy()
dict_gt = {occ[0]: occ[1] for occ in np_gt}
"""
"""
dict_nb_occ = {}
for occ in test.ids:
    sp = dict_gt[occ]
    if sp not in dict_nb_occ:
        dict_nb_occ[sp] = 1
    else:
        dict_nb_occ[sp] += 1

print(len(test))
n_ids = []
n_data = []
n_labels = []
for i, occ in enumerate(test.ids):
    if dict_nb_occ[dict_gt[occ]] >= 20:
        n_ids.append(occ)
        n_data.append(test.dataset[i])
        n_labels.append(test.labels[i])
test.ids = n_ids
test.dataset = n_data
test.labels = n_labels
print(len(test))
"""

batch_size = 16
test_loader = torch.utils.data.DataLoader(test, shuffle=False, batch_size=batch_size,
                                          num_workers=engine_configuration.nb_workers)

all_features = []
all_ids = []
for j, batch in enumerate(test_loader):
    idx = j * batch_size
    data, _ = batch
    output = model(data).cpu().detach().numpy()
    for k, op in enumerate(output):
        id = test.ids[batch_size*j+k]
        try:
            all_features.append(op.tolist())
            all_ids.append(id)
        except Exception as e:
            err = e

all_features = np.asarray(all_features)

np.save(output_path("logit_gardiole.npy"), all_features)

if PCA_SIZE > 0:
    all_features = PCA(n_components=PCA_SIZE).fit_transform(all_features)

features_embedded = TSNE(n_components=TSNE_SIZE, random_state=42).fit_transform(all_features)
np.save(output_path("tsne_gardiole_proba_pca_array_"+str(TSNE_SIZE)+".npy"), features_embedded)
np.save(output_path("tsne_gardiole_proba_pca_occ_ids.npy"), np.asarray(all_ids))
