import torch
import umap
import pandas as pd
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.neighbors import NearestNeighbors
from pyproj import Transformer, Proj
from ds.data.loader import occurrence_loader
from ds.ml.neural.checkpoints import load_or_create
from ds import logging as log

from ds_engine import engine_configuration

from dataset import DatasetGLC20
from models import InceptionEnv

from experiments.configs.inception import model_params
from ds.data.util.index import get_index
from ds_engine.util.path import output_path

import numpy as np

TSNE_SIZE = 3
PCA_SIZE = 50

source = 'glc20_train_fr'
engine_configuration.previous_execution = True

df_data = pd.read_csv("/home/benjamin/sdm_deploy/experiments/latefusion_20200609150900/occurrences_fr_train.csv", header='infer', sep=';', low_memory=False)
np_data = df_data.to_numpy()
np_data_ids = np.squeeze(np_data[:, 0])
np_data = np_data[:, 1:3]
print(np_data)

df_grid = pd.read_csv("/home/benjamin/sdm_deploy/experiments/latefusion_20200609150900/grid_points_1km.csv", header='infer', sep=';', low_memory=False)
np_grid = df_grid.to_numpy()
np_grid_ids = np.squeeze(np_grid[:, 2])
np_grid = np_grid[:, 0:2]
print(np_grid)

in_proj, out_proj = Proj(init='epsg:4326'), Proj(init='epsg:2154')
transformer = Transformer.from_proj(in_proj, out_proj)


def coord_transform(a):
    return transformer.transform(a[1], a[0])


np_data = np.apply_along_axis(coord_transform, 1, np_data)
np_grid = np.apply_along_axis(coord_transform, 1, np_grid)

nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(np_data)
distances, indices = nbrs.kneighbors(np_grid)

list_data = []
list_ids = []
list_labels = []
nb=0
for i, idx in enumerate(np_grid_ids):
    list_data.append(np.squeeze(np_data[indices[i]]).tolist())
    list_ids.append(int(np_data_ids[indices[i]][0]))
    list_labels.append(idx)
    """
    if distances[i] <= 1500:
        list_data.append(np.squeeze(np_data[indices[i]]).tolist())
        list_ids.append(int(np_data_ids[indices[i]][0]))
        list_labels.append(idx)
    if np.abs(np_data[indices[i], 0] - np_grid[i, 0]) <= 500.0 and np.abs(np_data[indices[i], 1] - np_grid[i, 1]) <= 500.0:
        list_data.append(np.squeeze(np_data[indices[i]]).tolist())
        list_ids.append(int(np_data_ids[indices[i]][0]))
        list_labels.append(idx)
    else:
        nb+=1
    """

print(np_grid_ids.shape[0])
print(nb)
print(nb/np_grid_ids.shape[0])

dataset = DatasetGLC20(list_labels, list_data, list_ids, "/home/data/GLC20/rasters/", "/home/data/extract_GLC20/GLC20_final_patches/", use_rasters=False)


model_params['n_input'] = dataset[0][0].shape[0]
model_params['n_labels'] = 31180
model_params['last_layer'] = False

model = load_or_create(model_class=InceptionEnv, model_params=model_params)
model.eval()

log.info('Dataset and model created... Exporting the results...')


index = get_index(output_path('index.json'))
batch_size = 16
dataset_loader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=batch_size,
                                          num_workers=engine_configuration.nb_workers)
print("predictions...")

all_features = []
all_ids = []
for j, batch in enumerate(dataset_loader):
    if j % 1000 == 999:
        print(j+1)
    idx = j * batch_size
    data, _ = batch
    output = model(data).cpu().detach().numpy()
    for k, op in enumerate(output):
        id = dataset.labels[batch_size*j+k]
        try:
            all_features.append(op.tolist())
            all_ids.append(id)
        except Exception as e:
            err = e

print("tsne...")

all_features = np.asarray(all_features)

if PCA_SIZE > 0:
    all_features = PCA(n_components=PCA_SIZE).fit_transform(all_features)
features_embedded = TSNE(n_components=TSNE_SIZE).fit_transform(all_features)

np.save(output_path("tsne_array_1km_nn.npy"), features_embedded)
np.save(output_path("tsne_1km_nn_occ_ids.npy"), np.asarray(all_ids))
